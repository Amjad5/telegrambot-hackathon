from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters
from credentials import TOKEN
import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

REPLY = 1


def c_start(bot, update):
    """ handler for /start command """

    update.message.reply_text('Welcome!\n'
                              'please use:\n'
                              '/question - to answer a question(the question will be requested separately)\n'
                              '/cancel - to stop the bot from waiting for question.')


def c_question(bot, update):
    """ handler for /question command
        asks the user to write a question to be answered
        - this method is the one responsible to start the conversation."""

    update.message.reply_text('Please, write down your question.\n'
                              'Questions should be written in clear English.\n'
                              'If you don\'t want to write a question anymore '
                              'please write /cancel to save memory space.')

    # move to the reply state in the conversation
    return REPLY


def get_reply(question):
    # todo: connect to AI and get answer
    return 'NONE'


def answer_question(bot, update):
    """ answers the question """

    message = update.message
    reply = get_reply(message.text)
    logger.info("User %s asked %s.", message.from_user.first_name, message.text)
    message.reply_text('the answer for\n%s\nIs:\n%s' % (message.text, reply))

    # end the conversation
    return ConversationHandler.END


def c_cancel(bot, update):
    """ handler for /cancel command
        stops the computer from waiting for a respond message. """
    logger.info("User %s canceled the conversation.", update.message.from_user.first_name)
    update.message.reply_text('OK, no question needed.')

    # end the conversation
    return ConversationHandler.END


def error_handler(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    updater = Updater(TOKEN)  # TOKEN is not available in this code. (you need to put your own)

    dispatcher = updater.dispatcher

    # connect command 'start' to c_start function
    dispatcher.add_handler(CommandHandler('start', c_start))

    conversation_handler = ConversationHandler(
        entry_points=[CommandHandler('question', c_question)],

        states={
            REPLY: [MessageHandler(Filters.text, answer_question)]
        },

        fallbacks=[CommandHandler('cancel', c_cancel)]
    )

    dispatcher.add_handler(conversation_handler)

    dispatcher.add_error_handler(error_handler)

    # start the bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
